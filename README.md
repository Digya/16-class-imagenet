# 16-class-ImageNet

This repository contains the 16-class ImageNet dataset downloaded from the [ImageNet URLs](http://www.image-net.org/download-imageurls).

The dataset follows the mapping of 16 level categories to their corresponding ImageNet labels using the WordNet hierarchy as mentioned in [Generalisation in humans and deep neural networks](https://arxiv.org/pdf/1808.08750.pdf).

The 16-categories are:
* airplane
* bicycle
* boat
* car
* chair
* dog
* keyboard
* oven
* bear
* bird
* bottle
* cat
* clock
* elephant
* knife
* truck

The mapping that I followed is [MSCOCO_to_ImageNet_category_mapping.txt](https://github.com/rgeirhos/generalisation-humans-DNNs/blob/master/16-class-ImageNet/MSCOCO_to_ImageNet_category_mapping.txt).